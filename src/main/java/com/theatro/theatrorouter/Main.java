/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theatro.theatrorouter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.net.*;
import java.util.Date;
import java.util.logging.LogRecord;

/**
 *
 * @author cmiller
 */
public class Main {
    public static final Logger LOGGER = Logger.getLogger("THEATRO_ROUTER");
    public static final String usage = 
    "\nstart - start statistics collection\n"+
    "stop - stop statistics collection\n"+
    "status - check the status of statistics collection and other system parameters"+
    "show - shows total messages per second\n"+
    "verbose - shows message counts per chain/store \n"+
    "help - show this messsage\n";

    private static String onTest() {
        return "Not Implemented";
    }
    
    private static String onCommand(String command) {
        String output;
        switch (command) {
            case "help":
                output = usage;
                break;
            case "start":
                RabbitRouter.enableStats();
                output = "TR Stats enabled\n";
                break;
            case "stop":
                RabbitRouter.disableStats();
                output = "TR Stats disabled\n";
                break;
            case "status":
                String state="disabled";
                if (RabbitRouter.stats()) {
                    state="enabled";
                }
                output = "TR Stats are " + state + "\n";
                output+=RabbitRouter.channels();
                break;
            case "s":
            case "show":
                output = RabbitRouter.curStats();
                break;
            case "t":
            case "test":
                output = onTest();
                break;
            case "c":
            case "conn":
            case "connections":
                output = RabbitRouter.curConnections();
                break;
            default: 
                output = "Invalid Command\n";
                break;
        }
        return output;
    }
    
    public static void main(String[] args) throws InterruptedException, IOException, Exception {
        FileHandler fileHandler = new FileHandler("/opt/theatro/logs/TheatroRouter.log", true);
        LOGGER.addHandler(fileHandler);
        fileHandler.setFormatter(new SimpleFormatter(){
          private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

          @Override
          public synchronized String format(LogRecord lr) {
              return String.format(format,
                      new Date(lr.getMillis()),
                      lr.getLevel().getLocalizedName(),
                      lr.getMessage()
              );
          }
        });
        fileHandler.setLevel(Level.ALL);
        LOGGER.setLevel(Level.ALL);
        
        LOGGER.log(Level.INFO, "Starting Theatro Router.");
        
        try {
            LOGGER.log(Level.INFO, "Initializing Rabbit.");
            RabbitRouter.InitRabbit();
        }
        catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Failed to initialize Rabbit MQ subsystem - {0} : {1}", new Object[]{e, e.getMessage()});
            throw e;
        }

        int port = 5550;
        String host="0.0.0.0";
        try {
            host = args[0];
            port = Integer.parseInt(args[1]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
        }
        String msg = "Theatro Router starting on host " + host + " port " + port;
        LOGGER.log(Level.INFO, msg);

        TheatroRouter.startAll(host, port);

        msg = "Theatro Router started on host " + host + " port " + port;
        LOGGER.log(Level.INFO, msg);

        DatagramSocket serverSocket = new DatagramSocket(5551);
        while(true)
        {
            byte[] receiveData = new byte[64];
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            String command = new String( receivePacket.getData());
            byte[] sendData = onCommand(command.trim()).getBytes();
            InetAddress IPAddress = receivePacket.getAddress();
            int sndport = receivePacket.getPort();
            DatagramPacket sendPacket =
            new DatagramPacket(sendData, sendData.length, IPAddress, sndport);
            serverSocket.send(sendPacket);
        }
    }    
}
