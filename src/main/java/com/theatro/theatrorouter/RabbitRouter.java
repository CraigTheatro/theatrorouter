/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theatro.theatrorouter;

import com.rabbitmq.client.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.java_websocket.WebSocket;

/**
 *
 * @author cmiller
 */

public class RabbitRouter extends Thread {
    static final Logger LOGGER = Logger.getLogger("THEATRO_ROUTER");
    static final int MAX_ROUTERS=4;
    static boolean DEBUG = false;
    
    private static final HashMap<String, WebSocket> SOCKS = new HashMap<String, WebSocket>();
    private static final HashMap<String, Channel> CHANNELS = new HashMap<String, Channel>();
    private static final HashMap<WebSocket, String> STORES = new HashMap<WebSocket, String>();
    private static final HashMap<String, Integer> MESSAGES_BY_STORE = new HashMap<String, Integer>();
    private static final HashMap<String, Integer> CONNECTIONS_BY_STORE = new HashMap<String, Integer>();
    private static final HashMap<String, Integer> DISCONNECTIONS_BY_STORE = new HashMap<String, Integer>();

    private static int total_messages = 0;
    private static boolean stats_enabled = false;
    private static Date startTime;

//    static String centralSubHost = "amqp://shiva:vanik123@central.theatro.com/theatrocentralvhost";
//    static String centralPubHost = "amqp://shiva:vanik123@central.theatro.com/theatrocentral";
//    static String influxPubHost = "amqp://shiva:vanik123@influx.theatro.com/theatrocentral";
//    static String centralSubHost = "amqp://shiva:vanik123@dallascentral.theatro.com/theatrocentralvhost";
//    static String centralPubHost = "amqp://shiva:vanik123@dallascentral.theatro.com/theatrocentral";
//    static String influxPubHost = "amqp://shiva:vanik123@dallascentral.theatro.com/theatrocentral";

//    static String centralHost="System.getenv("PRIMARY_RABBIT_SERVER"); //central.theatro.com
//   static String devstatHost=System.getenv("DEVSTAT_RABBIT_SERVER"); //influx.theatro.com
//   static String slmHost=System.getenv("SLM_RABBIT_SERVER"); // slm.theatro.com    
    static String centralSubHost = "amqp://shiva:vanik123@%s/theatrocentralvhost";
    static String centralPubHost = "amqp://shiva:vanik123@%s/theatrocentral";
    static String influxPubHost = "amqp://shiva:vanik123@%s/theatrocentral";
    static String slmPubHost = "amqp://shiva:vanik123@%s/theatrocentral";

    static ConnectionFactory centralSubFactory = new ConnectionFactory();
    static ConnectionFactory centralPubFactory = new ConnectionFactory();
    static ConnectionFactory influxPubFactory = new ConnectionFactory();
    static ConnectionFactory slmPubFactory = new ConnectionFactory();
    
    static RabbitRouter[] myRouters = new RabbitRouter[MAX_ROUTERS];
    static int nextRouter=0;
    static Boolean initialized = false;

    static Connection subconnection;
    
    static Connection pubconnection;
    static Channel pubchannel;

    static Connection ipubconnection;
    static Channel ipubchannel;

    static Connection slmpubconnection;
    static Channel slmpubchannel;

    static Connection tsmpubconnection;
    static Channel tsmpubchannel;

    String chainstore;
    WebSocket websocket;
    
    private final LinkedList<String> myMessages;
    private int messagesProcessed;
    static Integer numConnectionsDown = 0;

    private static class RabbitConnectionShutdownListener implements com.rabbitmq.client.ShutdownListener {

        @Override
        public void shutdownCompleted(ShutdownSignalException cause) {
            LOGGER.log(Level.SEVERE, String.format("Rabbit component has shut down %s", cause.getReference()));
            if (RabbitRouter.numConnectionsDown == 0) {
                TheatroRouter.stopAll();
            }
            RabbitRouter.numConnectionsDown++;
        }
    }

    private static class RabbitConnectionRecoveryListener implements com.rabbitmq.client.RecoveryListener {

        @Override
        public void handleRecoveryStarted(Recoverable recoverable) {
            LOGGER.log(Level.WARNING, String.format("Rabbit component recovery has started %s", recoverable));
        }
        
        @Override
        @SuppressWarnings("SynchronizeOnNonFinalField")
        public void handleRecovery(Recoverable recoverable) {
            synchronized (RabbitRouter.numConnectionsDown) {
                LOGGER.log(Level.WARNING, String.format("Rabbit component recovery has completed %s", recoverable));
                if (RabbitRouter.numConnectionsDown > 0) {
                    RabbitRouter.numConnectionsDown--;
                }
                if (RabbitRouter.numConnectionsDown == 0) {
                    TheatroRouter.startAll();
                    LOGGER.log(Level.WARNING, "TheatroRouter:  Restarted WebSocketServer!");
                }
            }
        }
    }


    public static void enableStats() {
        stats_enabled=true;
        startTime=new Date();
        total_messages=0;
        for (RabbitRouter r: myRouters) {
            r.messagesProcessed=0;
        }
    }
    
    public static void disableStats() {
        stats_enabled=false;
    }
    
    public static boolean stats() {
        return stats_enabled;
    }

    public static String channels() {
        String output = "pubchannel is: " + pubchannel + "\n";
        output = output + "ipubchannel is: " + ipubchannel + "\n";
        output = output + "slmpubchannel is: " + slmpubchannel + "\n";
        output = output + "tsmpubchannel is: " + tsmpubchannel + "\n";
        return output;
    }
    
    public static String curStats() {
        Date now = new Date();
        double seconds = (now.getTime()-startTime.getTime())/1000.0;
        double msgsper = (double)total_messages/seconds;
        String output = "Stats Started at: " + startTime + "\n";
        output=output+"Total Time: "+seconds+"\n";
        output=output+"Total Msgs: "+total_messages+"\n";
        output=output+"MPS: " + msgsper + "\n";

        output=output+"Q Size per thread: ";
        for (RabbitRouter r: myRouters) {
            output=output+r.myMessages.size()+" ";
        }
        output=output+"\n";

        output=output+"Msgs per thread: ";
        for (RabbitRouter r: myRouters) {
            output=output+r.messagesProcessed+" ";
        }
        output=output+"\n";
        if (MESSAGES_BY_STORE.size() != 0) {
            output = output + "-----------------------------------------\n";
            for (Map.Entry mapElement : MESSAGES_BY_STORE.entrySet()) { 
                String chainstore = (String)mapElement.getKey(); 
                Integer msgCount = (Integer)mapElement.getValue();
                output = output + String.format("%s: %d\n", chainstore, msgCount);
            }         
        }
        return output;
    }
    
    public static String curConnections() {
        String output = String.format("Currently Connected-(%d)\n", SOCKS.size());
        if (SOCKS.size() != 0) {
            output = output + "-----------------------------------------\n";
            for (Map.Entry mapElement : SOCKS.entrySet()) { 
                String chainstore = (String)mapElement.getKey(); 
                WebSocket ws = (WebSocket)mapElement.getValue(); 
                output = output + String.format("%s: %s\n", chainstore, ws);
            }         
        }
        return output;
    }

    private static Properties getProperties() {
        String userdir = System.getProperty("user.dir");
        System.out.println(userdir);
        Properties props = new Properties();
        InputStream in;
        try {
            in = new FileInputStream(userdir + File.separator + "theatrorouter.properties");
            props.load(in);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            return null;
        } 
        return props;
    }
    
    static void InitRabbit() throws Exception {
        try {
            Properties props = getProperties();
            String nbhost = props.getProperty("northbound.host");
            String sbhost = props.getProperty("southbound.host");
            String slmhost = props.getProperty("slm.host");
            String devhost = props.getProperty("device.host");
            DEBUG = Boolean.valueOf(props.getProperty("DEBUG"));
            centralPubFactory.setUri(String.format(centralPubHost, nbhost));
            centralSubFactory.setUri(String.format(centralSubHost, sbhost));
            influxPubFactory.setUri(String.format(influxPubHost, devhost));
            slmPubFactory.setUri(String.format(slmPubHost, slmhost));
            if (DEBUG) {
                LOGGER.log(Level.INFO, "Read properties");            
            }
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw ex;
        }

        try {
            // Subscriber exchange for Central
            subconnection = centralSubFactory.newConnection();
            ((Recoverable)subconnection).addRecoveryListener(new RabbitConnectionRecoveryListener());
            ((ShutdownNotifier)subconnection).addShutdownListener(new RabbitConnectionShutdownListener());
                    
            // Publisher exchange for Central
            pubconnection = centralPubFactory.newConnection();
            ((Recoverable)pubconnection).addRecoveryListener(new RabbitConnectionRecoveryListener());
            ((ShutdownNotifier)pubconnection).addShutdownListener(new RabbitConnectionShutdownListener());

            pubchannel = pubconnection.createChannel();
            pubchannel.exchangeDeclare("northbounddata", "topic");

            // Publisher exchange for Influx
            ipubconnection = influxPubFactory.newConnection();
            ((Recoverable)ipubconnection).addRecoveryListener(new RabbitConnectionRecoveryListener());
            ((ShutdownNotifier)ipubconnection).addShutdownListener(new RabbitConnectionShutdownListener());

            ipubchannel = ipubconnection.createChannel();
            ipubchannel.exchangeDeclare("northbounddata", "topic");

            // Publisher exchange for SLM
            slmpubconnection = slmPubFactory.newConnection();
            ((Recoverable)slmpubconnection).addRecoveryListener(new RabbitConnectionRecoveryListener());
            ((ShutdownNotifier)slmpubconnection).addShutdownListener(new RabbitConnectionShutdownListener());

            slmpubchannel = slmpubconnection.createChannel();
            slmpubchannel.exchangeDeclare("northbounddata", "topic");

            // Publisher exchange for TSM
            tsmpubconnection = centralPubFactory.newConnection();
            tsmpubchannel = tsmpubconnection.createChannel();
            tsmpubchannel.exchangeDeclare("tsm", "topic");

        } catch (IOException | TimeoutException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        LOGGER.log(Level.INFO, String.format("Northbound Pubchannel is - %s",pubchannel));
        LOGGER.log(Level.INFO, String.format("Influx Pubchannel is - %s", ipubchannel));
        LOGGER.log(Level.INFO, String.format("SLM Pubchannel is - %s", slmpubchannel));
        LOGGER.log(Level.INFO, String.format("TSM Pubchannel is - %s", tsmpubchannel));
        
        // Create a few RabbitRouters for the northbound CHANNELS.
        for (int i = 0; i < MAX_ROUTERS; i++) {
            myRouters[i] = new RabbitRouter();
            myRouters[i].start();
        }
        
        initialized = true;
    }
    
    static void logMessage(RabbitRouter rr, WebSocket ws) {
        if (stats_enabled) {
            String cs = STORES.get(ws);
            if (cs != null) {
                int count = MESSAGES_BY_STORE.containsKey(cs) ? MESSAGES_BY_STORE.get(cs) : 0;
                MESSAGES_BY_STORE.put(cs, count+1);
                total_messages +=1;
                rr.messagesProcessed += 1;
            }
        }
    }
    
    static void newMessage(String message, WebSocket ws) {
        if (message.startsWith("I")) {
            onInit(message, ws);
        }
        else { 
            synchronized (myRouters) {
                RabbitRouter rr = myRouters[nextRouter];
                logMessage(rr, ws);
                synchronized (rr.myMessages) {
                    rr.myMessages.add(message);
                    rr.myMessages.notify();
                }
                if (++nextRouter >= MAX_ROUTERS) nextRouter=0;
            }
        }
    }

    static void logInit(String cs) {
        if (stats_enabled) {
            int count = CONNECTIONS_BY_STORE.containsKey(cs) ? CONNECTIONS_BY_STORE.get(cs) : 0;
            CONNECTIONS_BY_STORE.put(cs, count+1);
            count = MESSAGES_BY_STORE.containsKey(cs) ? MESSAGES_BY_STORE.get(cs) : 0;
            MESSAGES_BY_STORE.put(cs, count+1);
            total_messages +=1;
        }
    }
    
    // Init is the first message sent to identify chain and store.
    static void onInit(String message, WebSocket ws) {
        String chainstore = message.substring(2);
        
        // Delete old stuff just in case for safety.
        try {
            String key=STORES.remove(ws);
            WebSocket data=SOCKS.remove(key);
            Channel channel = CHANNELS.remove(key);
            if (channel != null) {
                channel.close();
            }
        }
        catch (IOException | TimeoutException e) {
            LOGGER.log(Level.INFO, String.format("Old connection was cleaned up correctly for %s", chainstore));
        }
        SOCKS.put(chainstore, ws);
        STORES.put(ws, chainstore);
        logInit(chainstore);
        try {
            Channel channel = subconnection.createChannel();
            CHANNELS.put(chainstore, channel);
            channel.exchangeDeclare("aniket", "direct");
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, "aniket", chainstore);
            LOGGER.log(Level.INFO, String.format("Binding aniket to - %s at %s", queueName, chainstore));
            // Create receiver thread for subscriber channel.
            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                                           AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String routingKey = envelope.getRoutingKey();
                    String contentType = properties.getContentType();
//                    long deliveryTag = envelope.getDeliveryTag();
//                    LOGGER.log(Level.INFO, String.format("Message for delivery: %s Content Type: %s Message: %s", routingKey, contentType, new String(body)));
                    WebSocket ws = SOCKS.get(routingKey);
                    if (ws != null) {
                        ws.send(body);
                    }
                    else {
                        LOGGER.log(Level.SEVERE, "Message received for store that is offline.");
                    }
                }
            };
            channel.basicConsume(queueName, true, consumer);
        }
        catch (IOException e) {
            LOGGER.log(Level.INFO, String.format("Error creating read exchange for conn=%s : name=%s", ws, chainstore));
        }

        String addr = ws.getRemoteSocketAddress().getAddress().getHostAddress();
        String[] cs=chainstore.split("/", 2);
        String chain=cs[0];
        String store=cs[1];
        String tsm_state = String.format("T|%s.%s.data|{\"msg_type\": \"TSM_State\", \"connected\": true, \"chain\": \"%s\", \"store\": \"%s\", \"external_ip\": \"%s\", \"internal_ip\": \"%s\"}", chain, store, chain, store, addr, addr);
        newMessage(tsm_state, null);
        LOGGER.log(Level.INFO, String.format("Store is online - conn=%s : name=%s", ws, chainstore));
    }

    static void clearLEDs(String key) {
        String[] chainstore=key.split("/", 2);
        String chain=chainstore[0];
        String store=chainstore[1];
        Instant instant = Instant.now();
        long timeStampSeconds = instant.getEpochSecond();
        String tgs_state = String.format("T|%s.%s.data|{\"msg_type\": \"TGS_State\", \"tag_heartbeat\": false, \"pubsub_heartbeat\": false, \"connected\": false, \"date\": \"10/02/2018\", \"tgs_heartbeat\": false, \"chain\": \"%s\", \"sidecar_heartbeat\": false, \"hmm_heartbeat\": false, \"time\": %d.0, \"store\": \"%s\"}", chain, store, chain, timeStampSeconds, store);
        newMessage(tgs_state, null);
        String tsm_state = String.format("T|%s.%s.data|{\"msg_type\": \"TSM_State\", \"connected\": false, \"chain\": \"%s\", \"store\": \"%s\"}", chain, store, chain, store);
        newMessage(tsm_state, null);
    }
    
    static void onClose(WebSocket ws) {
        try {
            String key=STORES.remove(ws);
            WebSocket data=SOCKS.remove(key);
            if (ws != data) {
                LOGGER.log(Level.INFO, String.format("Removed Incorrect Connection - conn=%s : data=%s : key=%s", ws, data, key));
            }
            else {
               try {
                    clearLEDs(key);
                    Channel channel = CHANNELS.remove(key);
                    channel.close();
                    LOGGER.log(Level.INFO, String.format("Removed Connection - %s", key));
                } catch (IOException | TimeoutException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        } 
        catch (Exception e) {
            LOGGER.log(Level.WARNING, String.format("Attempting to close unknown websocket %s", ws));
        }
    }
  
    public RabbitRouter() {
        myMessages=new LinkedList<>();
        messagesProcessed=0;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String message;
                synchronized (myMessages) {
                    while (myMessages.isEmpty()) {
                        try {
                            myMessages.wait();
                        } catch (InterruptedException e) {
                            LOGGER.log(Level.INFO, String.format("An error occurred while queue is waiting: %s", e.getMessage()));
                        }
                    }
                    message = myMessages.poll();
                }
                onMessage(message);
            }
            catch (Exception e) {
                LOGGER.log(Level.INFO, "An error occurred while processing messages", e);
            }
        }
    }

    void onMessage(String message) {
        try {
            // Messages are of the format "X|chain.store.route|{some json message}"
            // Where X is the key to tell us what channel.
            String exchId=message.substring(0, 1);
            int nextSep=message.indexOf('|', 2);
            String key = message.substring(2, nextSep);
            String jsonstring = message.substring(nextSep+1);
            Channel channel;
            String exchangeName;
            exchangeName="northbounddata";
            switch (exchId) {
                case "T":
                    channel=tsmpubchannel;
                    exchangeName="tsm";
                    break;
                case "D":
                    channel=ipubchannel;
                    break;
                case "S":
                    channel=slmpubchannel;
                    break;
                case "R":
                    channel=pubchannel;
                    break;
                default:
                    LOGGER.log(Level.WARNING, "Invalid exchange ID received: {0}", exchId);
                    return;
            }
            try {
                channel.basicPublish(exchangeName, key, null, jsonstring.getBytes());
                LOGGER.log(Level.INFO, String.format("message sent. %s, %s, %s, %s", exchangeName, key, message, channel));            
            } catch (java.lang.NullPointerException n) {
                LOGGER.log(Level.WARNING, String.format("NullPointerException %s, %s, %s, %s", exchangeName, key, message, channel));            
            }
        } catch (IOException | java.lang.NullPointerException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    public void close() throws TimeoutException, IOException {
        LOGGER.log(Level.WARNING, "Rabbit Router received close()");            
//        subchannel.close();
        subconnection.close();

        pubconnection.close();
        pubchannel.close();

        ipubconnection.close();
        ipubchannel.close();

        slmpubconnection.close();
        slmpubchannel.close();

        tsmpubconnection.close();
        tsmpubchannel.close();
    }    
}
