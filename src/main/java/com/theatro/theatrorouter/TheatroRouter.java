
package com.theatro.theatrorouter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

/**
 * TheatroRouter Handles exchange between stores and RabbitMQ on the Central server.
 */
public class TheatroRouter extends WebSocketServer {
    static final Logger LOGGER = Logger.getLogger("THEATRO_ROUTER");
    static TheatroRouter router = null;
    static String host;
    static int port;
    
    public TheatroRouter(String host, int port) throws UnknownHostException {
        super(new InetSocketAddress(host, port));
        router=this;
    }

    public static void stopAll() {
        if (router != null) {
            try {
                router.stop();
                router = null;
            }
            catch (IOException | InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "Could not stop WebSocketServer: ", ex);                
            }
        }
    }
    
    public static void startAll() {
        if (router == null) {
            try {
                router = new TheatroRouter(host, port);
                router.setReuseAddr( true );
                router.start();
            }
            catch (UnknownHostException ex) {
                LOGGER.log(Level.SEVERE, "Could not start WebSocketServer: ", ex);                
            }
        }        
    }

    public static void startAll(String h, int p) {
        host = h;
        port = p;
        startAll();
    }

    public TheatroRouter(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        LOGGER.log(Level.INFO, String.format("%s connected!", conn.getRemoteSocketAddress().getAddress().getHostAddress()));
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        LOGGER.log(Level.WARNING, String.format("%s has gone away! Reason: %s Code: %d", conn.getRemoteSocketAddress().getAddress().getHostAddress(), reason, code));
        RabbitRouter.onClose(conn);
    }
    
    @Override
    public void onMessage(WebSocket conn, String message) {
        RabbitRouter.newMessage(message, conn);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        LOGGER.log(Level.INFO, String.format("ByteBuffer Message Received from: %s: %s", conn.getRemoteSocketAddress().getAddress().getHostAddress(), message));
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        if (conn != null) {
            LOGGER.log(Level.WARNING, String.format("WebSocket Error on %s.  Exception %s.  Allowing Rabbit Router to clean up.", conn, ex));
//            conn.close();
            RabbitRouter.onClose(conn);
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        LOGGER.info("Server started!");
        setConnectionLostTimeout(30);
    }
}
